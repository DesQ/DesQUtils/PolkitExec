/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <iostream>
#include <QtCore>

#include "options.hpp"

void printHelp() {
    std::cout << "DesQPKit" << "v1.0.0" << std::endl;
    std::cout << "PolicyKit based helper for performing internal actions with elevated privileges" << std::endl << std::endl;
    std::cout << "Usage:" << std::endl;
    std::cout << "desq-pkit [options]" << std::endl << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "  -h, --help                        " << "Displays this help message." << std::endl;
    std::cout << "  --help-all                        " << "Displays help including Qt specific options." << std::endl;
    std::cout << "  -v, --version                     " << "Displays version information." << std::endl;
    std::cout << "  --brightness <brightness> device  " << "Set the brightness <brightness> of the <device>" << std::endl;
}


int main( int argc, char *argv[] ) {
    QCoreApplication app( argc, argv );

    app.setApplicationName( "DesQPKit" );
    app.setOrganizationName( "DesQ" );
    app.setApplicationVersion( "1.0.0" );

    QCommandLineParser cliparser;

    cliparser.addHelpOption();         // Help
    cliparser.addVersionOption();      // Version

    // Description
    cliparser.setApplicationDescription( "PolicyKit based helper for performing internal actions with elevated privileges" );

    cliparser.addOption( { "brightness", "Set the brightness of the lcd in percentage.", "brightness" } );
    cliparser.process( app );

    /* If no arguments are sepcified */
    if ( argc == 1 ) {
        printHelp();
        exit( EXIT_SUCCESS );
    }

    if ( cliparser.isSet( "brightness" ) ) {
        QString brightnessValue( cliparser.value( "brightness" ) );
        setBrightness( cliparser.value( "brightness" ), cliparser.positionalArguments() );
    }
}
