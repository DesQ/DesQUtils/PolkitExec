# DesQ Polkit Exec
DesQ PolkitExec is the an executable to run certain selected root commands without the user having to enter the root passwords

### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/PolkitExec.git DesQPolkitExec`
- Enter the `DesQPolkitExec` folder
  * `cd DesQPolkitExec`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`

### Dependencies:
 * Qt5 (qtbase5-dev, qtbase5-dev-tools)

## My System Info
 * OS:				Debian Sid
 * Qt:				Qt5 5.12.5

### Known Bugs
 * --

### Upcoming
 * Any other feature you request for... :)
