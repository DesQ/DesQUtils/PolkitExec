/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <QtCore>
#include <unistd.h>

int getBrightness( QString rawVal, QString dev ) {
    /** Max Brightness */
    QFile device( "/sys/class/backlight/" + dev + "/max_brightness" );

    device.open( QFile::ReadOnly );

    int max = device.readAll().toInt();

    device.close();

    /** Current Brightness */
    device.setFileName( "/sys/class/backlight/" + dev + "/brightness" );
    device.open( QFile::ReadOnly );

    int cur = device.readAll().toInt();

    device.close();

    int val = 1;

    /** Change relative to the current value */
    if ( rawVal.startsWith( "-" ) or rawVal.startsWith( "+" ) ) {
        /** Percentage change */
        if ( rawVal.endsWith( "%" ) ) {
            val = cur + max * rawVal.replace( "%", "" ).toInt() / 100;

            /** Ensure 1 <= val <= max */
            val = (val > max ? max : (val <= 0 ? 1 : val) );
        }

        /** Absolute change */
        else {
            val = cur + rawVal.toInt();

            /** Ensure 1 <= val <= max */
            val = (val > max ? max : (val <= 0 ? 1 : val) );
        }
    }

    /** Set the absolute value */
    else {
        /** Percentage of the maximum brightness */
        if ( rawVal.endsWith( "%" ) ) {
            val = max * rawVal.replace( "%", "" ).toInt() / 100;

            /** Ensure 1 <= val <= max */
            val = (val > max ? max : (val == 0 ? 1 : val) );
        }

        else {
            val = rawVal.toInt();

            /** Ensure 1 <= val <= max */
            val = (val > max ? max : (val == 0 ? 1 : val) );
        }
    }

    /** Return the value we've deduced from the cli argument */
    return val;
}


void setBrightness( QString rawVal, QStringList devices ) {
    /** No devices were set: Change brightness of everything */
    if ( not devices.count() ) {
        QDirIterator it( "/sys/class/backlight/", QDir::Dirs | QDir::NoDotAndDotDot );
        while ( it.hasNext() ) {
            /** Get cur brightness */
            QFile device( it.next() + "/brightness" );
            device.open( QFile::ReadOnly );

            int cur = device.readAll().toInt();
            device.close();

            int val = getBrightness( rawVal, it.fileName() );

            /** Prepare device for writing */
            device.open( QFile::WriteOnly );

            /** Get delta: +1 if val > cur else -1 */
            int delta = (val > cur ? 1 : -1);

            while ( cur != val ) {
                device.write( QByteArray::number( cur ) );
                device.flush();

                cur += delta;

                /** 0.5 ms sleep */
                usleep( 500 );
            }

            device.close();
        }
    }

    else {
        for ( QString dev: devices ) {
            /** Get cur brightness */
            QFile device( "/sys/class/backlight/" + dev + "/brightness" );
            device.open( QFile::ReadOnly );

            int cur = device.readAll().toInt();
            device.close();

            int val = getBrightness( rawVal, dev );

            /** Prepare device for writing */
            device.open( QFile::WriteOnly );

            /** Get delta: +1 if val > cur else -1 */
            int delta = (val > cur ? 1 : -1);

            while ( cur != val ) {
                cur += delta;

                device.write( QByteArray::number( cur ) );
                device.flush();

                /** 0.5 ms sleep */
                usleep( 500 );
            }

            device.close();
        }
    }
}
